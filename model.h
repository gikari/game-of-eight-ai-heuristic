#pragma once

#include <QObject>
#include <QVector>
#include <QSet>

#include <QDebug>

#include <algorithm>

namespace GameOfEight {

using CellAddress = std::pair<int, int>;
using CellValue = int;
using GameState = QVector<QVector<int>>;

class Model : public QObject
{
    Q_OBJECT
public:
    enum class Direction {UP, RIGHT, DOWN, LEFT};
    explicit Model(QObject *parent = nullptr);

public:
    void solve();
    void reset();

private:
    template<typename Func>
    void solveWithHeuristic(Func && heuristic) {
        reset();
        while (!isWin()) {
            qDebug() << "\n============================";
            printCurrentState();
            qDebug() << "Current heuristic:" << currentStateHeuristic;
            qDebug() << "============================";
            solveAStar(heuristic);
        }

        qDebug() << "VICTORY!";
        qDebug() << "\n============================";
        printCurrentState();
        qDebug() << "Current heuristic:" << currentStateHeuristic;
        qDebug() << "============================";

        qDebug() << "Total moves:" << boardMoves;
    }

    template<typename Func>
    void solveAStar(Func&& heuristic) {
        boardMoves++;

        QVector<GameState> newStatesToAnalyze {getPossibleFutureStates()};
        QVector<int> newStatesHeuristics {};
        for (GameState state : newStatesToAnalyze) {
            newStatesHeuristics.append(heuristic(state));
        }

        statesToAnalyze += newStatesToAnalyze;
        statesToAnalyzeHeuristics += newStatesHeuristics;

        int minHeuristicsIndex = static_cast<int>(std::min_element(newStatesHeuristics.begin(), newStatesHeuristics.end()) - newStatesHeuristics.begin());

        currentState = std::move(statesToAnalyze[minHeuristicsIndex]);
        currentStateHeuristic = std::move(statesToAnalyzeHeuristics[minHeuristicsIndex]);

        statesToAnalyze.remove(minHeuristicsIndex);
        statesToAnalyzeHeuristics.remove(minHeuristicsIndex);

        visitedStates += currentState;
    }

    int getNumberOfCellsInWrongPlace(const GameState &state);
    int getSumOfCellsManhattanDistance(const GameState &state);

    void printCurrentState();

    bool isWin();

    char getCellChar(int row, int column);

    CellAddress getCellAddress(int cellValue);
    CellAddress getCellAddress(int cellValue, const GameState &state);

    CellAddress getWinningCellAddress(int cellValue);
    int getManhattanDistance(const CellAddress &currentCellPosition, const CellAddress &winningCellPossition);

    QVector<GameState> getPossibleFutureStates();

    CellValue getNeighboor(const CellAddress &cellAddress, const Direction &direction);
    CellAddress getNeighboorAddress(const CellAddress &cellAddress, const Direction &direction);
    GameState makeMoveInCurrentState(const Direction &direction);




    QVector<GameState> statesToAnalyze;
    QVector<int> statesToAnalyzeHeuristics;
    GameState currentState;
    int currentStateHeuristic;

    QSet<GameState> visitedStates;
    int boardMoves;
};

}
