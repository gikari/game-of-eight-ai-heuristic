#include "model.h"
#include <QDebug>
#include <QVector>
#include <vector>

namespace GameOfEight {

Model::Model(QObject *parent) : QObject(parent),
    statesToAnalyze{},
    statesToAnalyzeHeuristics{},
    currentState{
        {8, 7, 3},
        {1, 5, 6},
        {4, 2, 0}
        },
    currentStateHeuristic{},
    visitedStates{},
    boardMoves{}
{

}

void Model::solve()
{
    auto heuristicFunctionOne = [this](const GameState &state) {
        return this->getNumberOfCellsInWrongPlace(state);
    };

    auto heuristicFunctionTwo = [this](const GameState &state) {
        return this->getSumOfCellsManhattanDistance(state);
    };

    solveWithHeuristic(heuristicFunctionOne);
    int firstHeuristicTotalMoves {boardMoves};
    solveWithHeuristic(heuristicFunctionTwo);
    int secondHeuristicTotalMoves {boardMoves};

    qDebug() << "Stats:";
    qDebug() << "  First heuristic (Amount of bad cells):";
    qDebug() << "    Moves:" << firstHeuristicTotalMoves;
    qDebug() << "  Second heuristc (Sum of Manhattan distances):";
    qDebug() << "    Moves:" << secondHeuristicTotalMoves;
}

void Model::reset()
{
    currentState ={
        {8, 7, 3},
        {1, 5, 6},
        {4, 2, 0}
    };
    currentStateHeuristic = -1;
    visitedStates.clear();
    statesToAnalyze.clear();
    statesToAnalyzeHeuristics.clear();
    boardMoves = 0;
}

int Model::getNumberOfCellsInWrongPlace(const GameState &state)
{
    int numberOfWrongCells {};
    if (state[0][0] != 1) {
        numberOfWrongCells++;
    }
    if (state[0][1] != 2) {
        numberOfWrongCells++;
    }
    if (state[0][2] != 3) {
        numberOfWrongCells++;
    }
    if (state[1][0] != 4) {
        numberOfWrongCells++;
    }
    if (state[1][1] != 5) {
        numberOfWrongCells++;
    }
    if (state[1][2] != 6) {
        numberOfWrongCells++;
    }
    if (state[2][0] != 7) {
        numberOfWrongCells++;
    }
    if (state[2][1] != 8) {
        numberOfWrongCells++;
    }
    return numberOfWrongCells++;
}

int Model::getSumOfCellsManhattanDistance(const GameState &state)
{
    int sumOfDistances {};
    for (int cellValue = 1; cellValue <= 8; ++cellValue) {
        CellAddress currentCellPosition = getCellAddress(cellValue, state);
        CellAddress winningCellPossition = getWinningCellAddress(cellValue);
        int cellDistance {getManhattanDistance(currentCellPosition, winningCellPossition)};

        sumOfDistances += cellDistance;
    }
    return sumOfDistances;
}

char Model::getCellChar(int row, int column)
{
    int cellValue {-1};
    if (row >= 0 && row <= 2 && column >= 0 && column <= 2) {
        cellValue = currentState[row][column];
        return cellValue == 0 ? ' ' : QString::number(cellValue).toStdString()[0];
    } else {
        return 'X';
    }
}

void Model::printCurrentState()
{
    qDebug() << "Current state:\n\n"
             << getCellChar(0, 0) << "|" << getCellChar(0, 1) << "|" << getCellChar(0, 2) << "\n"
             << "--+---+--\n"
             << getCellChar(1, 0) << "|" << getCellChar(1, 1) << "|" << getCellChar(1, 2) << "\n"
             << "--+---+--\n"
             << getCellChar(2, 0) << "|" << getCellChar(2, 1) << "|" << getCellChar(2, 2) << "\n" ;
}


QVector<GameState> Model::getPossibleFutureStates()
{
    QVector<GameState> futureStates {};

    CellAddress zeroCellCoordinates = getCellAddress(0);
    QVector<Direction> directions {Direction::UP, Direction::RIGHT, Direction::DOWN, Direction::LEFT};
    for (Direction direction : directions) {
        if (getNeighboor(zeroCellCoordinates, direction) != -1) {
            GameState newState = makeMoveInCurrentState(direction);
            if (!visitedStates.contains(newState)) {
                futureStates += newState;
            }
        }
    }
    return futureStates;
}

bool Model::isWin()
{
    return currentState[0][0] == 1
        && currentState[0][1] == 2
        && currentState[0][2] == 3
        && currentState[1][0] == 4
        && currentState[1][1] == 5
        && currentState[1][2] == 6
        && currentState[2][0] == 7
            && currentState[2][1] == 8;
}

CellAddress Model::getCellAddress(int cellValue)
{
    return getCellAddress(cellValue, currentState);
}

CellAddress Model::getCellAddress(int cellValue, const GameState &state)
{
    for (int i = 0; i < state.size(); i++) {
        for (int j = 0; j < state[i].size(); j++) {
            if (state[i][j] == cellValue) {
                return {i, j};
            }
        }
    }
    return {-1, -1};
}

CellAddress Model::getWinningCellAddress(int cellValue)
{
    if (cellValue == 0) {
        return {2, 2};
    } else if (cellValue == 1) {
        return {0, 0};
    } else if (cellValue == 2) {
        return {0, 1};
    } else if (cellValue == 3) {
        return {0, 2};
    } else if (cellValue == 4) {
        return {1, 0};
    } else if (cellValue == 5) {
        return {1, 1};
    } else if (cellValue == 6) {
        return {1, 2};
    } else if (cellValue == 7) {
        return {2, 0};
    } else if (cellValue == 8) {
        return {2, 1};
    } else {
        return {-1, -1};
    }
}

int Model::getManhattanDistance(const CellAddress &currentCellPosition, const CellAddress &winningCellPossition)
{
    int rowDistance = std::abs(currentCellPosition.first - winningCellPossition.first);
    int columnDistance = std::abs(currentCellPosition.second - winningCellPossition.second);
    return rowDistance + columnDistance;
}

CellValue Model::getNeighboor(const CellAddress &cellAddress, const Model::Direction &direction)
{
    CellAddress neighboorAddress {getNeighboorAddress(cellAddress, direction)};
    if (neighboorAddress.first < 3 && neighboorAddress.first > -1
            && neighboorAddress.second < 3 && neighboorAddress.second > -1) {
        return currentState[neighboorAddress.first][neighboorAddress.second];
    } else {
        return -1;
    }
}

CellAddress Model::getNeighboorAddress(const CellAddress &cellAddress, const Model::Direction &direction)
{
    int rowNumber = cellAddress.first;
    int columnNumber = cellAddress.second;
    switch (direction) {
        case Direction::UP:
            return {rowNumber - 1, columnNumber};
        case Direction::RIGHT:
            return {rowNumber, columnNumber + 1};
        case Direction::DOWN:
            return {rowNumber + 1, columnNumber};
        case Direction::LEFT:
            return {rowNumber, columnNumber - 1};
    }
    return {-1, -1};
}

GameState Model::makeMoveInCurrentState(const Direction &direction)
{
    CellAddress zeroCellAddress = getCellAddress(0);
    int neighboorValue {getNeighboor(zeroCellAddress, direction)};
    if (neighboorValue == -1) {
        return {};
    } else {
        GameState futureState = currentState;
        CellAddress neighboorAddress = getNeighboorAddress(zeroCellAddress, direction);
        int tmp = futureState[zeroCellAddress.first][zeroCellAddress.second];
        futureState[zeroCellAddress.first][zeroCellAddress.second] = neighboorValue;
        futureState[neighboorAddress.first][neighboorAddress.second] = tmp;
        return futureState;
    }
}

}
